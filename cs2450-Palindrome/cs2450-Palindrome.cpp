// cs2450-Palindrome.cpp : Defines the entry point for the console application.

/*For this project, you will need to write a program which finds that longest
palindrome in a sequence of letters. Your program should prompt for a file, 
read its entire contents, and the output the longest palindrome found.

 VVAJASLEFJYABBZBBADOVDJXSOJGRJLALASD

the longest palindrome is ABBZBBA. Also note that there are other palindromes 
such as AJA, but we only want the longest.

For the sake of simplicity, you may assume that the palindrome is an odd length
(i.e. there will always be a center letter) and that the letters will all be 
uppercase (you won�t need to worry about comparing A to a). Additionally, you 
may assume that there will be less than 1000 characters in the sequence.

When reading in the file, the text may be on multiple lines, but you should
treat this as one long sequence and ignore newlines or carriage returns. */

#include "stdafx.h"

using namespace std;

//prototypes
static bool isPalindrome(string);
static string findLongestPalindrome(string);

int main()
{
	string filename;
	string data;
	string result;
	
	cout << "Please type in the name of the local text file: ";
	cin >> filename;
	ifstream myfile (filename);
	if (myfile.is_open())
	{
		while (myfile.good())
		{
			getline(myfile, data);
			cout << data << endl;
		}
		myfile.close();
	}
	else
		cout << "Unable to open file" << endl;
	
	result = findLongestPalindrome(data);

	cout << "The longest palindromic substring is " << result << endl;

	system("PAUSE");
	return 0;
}

static string findLongestPalindrome(string s)
{
	string testString = "";
	string currentMax = "";

	for (int i = 0; i < s.size(); i++)
	{
		for (int j = (i + 1); j < (s.size() - i); j++)
		{
			testString = s.substr(i,(j-i));
			if (isPalindrome(testString) == true)
			{
				if(testString.size() > currentMax.size())
				{
					currentMax = testString;
				}
			}

		}
	}
	return currentMax;
}

//Takes in a string, returns true or false
static bool isPalindrome(string s)
{
	 if(equal(s.begin(), s.begin() + s.size()/2, s.rbegin()))
        return true;
    else
        return false;
}

